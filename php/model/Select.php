<?php
require_once("Sql.php");
require_once("Cliente.php");


class Select {
    
    public function loadById($id){
        $sql = new Sql();
        $cliente = new Cliente();

        $results = $sql->select("SELECT * FROM tbclientes WHERE idcliente = :ID", array(":ID"=>$id));

        // verifica se a variavel results encontra alguma coisa la no banco.
        if(count($results) > 0){
            $cliente->setData($results[0]);
            echo $cliente->imprimir();
        }
    }
    //serve para buscar o id do cliente atrávez do numero
    public static function getByNum($numtelefone){
        $sql = new Sql();
        return $sql->select("SELECT idcliente FROM tbclientes WHERE numtelefone = :NUMTELEFONE", array(":NUMTELEFONE"=>$numtelefone));
    }
    public static function getNumtelefone($numtelefone){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbclientes WHERE numtelefone = :NUMTELEFONE", array(":NUMTELEFONE"=>$numtelefone));
    }
    public static function getCompras($id){
        $sql = new Sql();
        return $sql->select("SELECT numcompras FROM tbclientes WHERE idcliente = :ID", array(":ID"=>$id));
    }
    public static function getFone($id){
        $sql = new Sql();
        return $sql->select("SELECT numtelefone FROM tbclientes WHERE idcliente = :ID", array(":ID"=>$id));
    }
    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbclientes "); 
    }
    public static function search($nome){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbclientes WHERE nome LIKE :SEARCH ORDER BY nome", array(
            ':SEARCH'=>"%".$nome."%"
        ));
    }
    public static function searchUser($nome){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbusuarios WHERE nomeusu LIKE :SEARCH ORDER BY nomeusu", array(
            ':SEARCH'=>"%".$nome."%"
        ));
    }
    public static function getUsuario($id){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbusuarios WHERE idusuario = :ID", array(":ID"=>$id));
    }
    public static function getNameId(){
        $sql = new Sql();
        return $sql->select("SELECT idusuario, nomeusu FROM tbusuarios");
    }
    public static function getMensagem(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tbmensagens ");
    }
}