<?php

require_once("Sql.php");

class Delete {

    public function deletar($cliente){
        
        $sql = new Sql();

        $sql->query("DELETE FROM tbclientes WHERE idcliente = :ID", array(':ID'=>$cliente->getIdcliente()));

    }
    public static function deletarMensagem($id){
        $sql = new Sql();

        $sql->query("DELETE FROM tbmensagens WHERE idmensagem = :ID", array(':ID'=>$id));   
    }
}