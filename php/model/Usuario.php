<?php

class Usuario {
    private $idusuario;
    private $nomeusu;
    private $senhausu;
    private $funcao;
    private $createdate;

    public function getIdUsuario(){
        return $this->idusuario;
    }
    public function setIdUsuario($value){
        $this->idusuario = $value;
    }
    public function getNomeusu(){
        return $this->nomeusu;
    }
    public function setNomeusu($value){
        $this->nomeusu = $value;
    }
    public function getSenhausu(){
        return $this->senhausu;
    }
    public function setSenhausu($value){
        $this->senhausu = $value;
    }
    public function getFuncao(){
        return $this->funcao;
    }
    public function setFuncao($value){
        $this->funcao = $value;
    }
    public function getCreatedate(){
        return $this->createdate;
    }
    public function setCreatedate($value){
        $this->createdate = $value;
    }

    public function setData($data){
    
        $this->setIdUsuario($data['idusuario']);
        $this->setNomeusu($data['nomeusu']);
        $this->setSenhausu($data['senhausu']);
        $this->setFuncao($data['funcao']);
        $this->setCreatedate(new DateTime($data['createdate']));
    }
} 