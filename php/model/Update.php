<?php

require_once("Sql.php");
require_once("Cliente.php");


class Update {
    public function alteracao($cliente){
        $sql = new Sql();

        $sql->query("UPDATE tbclientes SET nome = :NOME, numtelefone = :NUMTELEFONE, numcompras = :NUMCOMPRAS WHERE idcliente = :ID", array(
            ":ID"=> $cliente->getIdcliente(),
            ":NOME"=> $cliente->getNome(),
            ":NUMTELEFONE"=> $cliente->getNumcompras(),
            ":NUMCOMPRAS"=> $this->getNumtelefone()
        ));
    }
    public static function alteracaoCompras($id, $numcompras){
        $sql = new Sql();

        $sql->query("UPDATE tbclientes SET numcompras = :NUMCOMPRAS WHERE idcliente = :ID", array(":NUMCOMPRAS"=>$numcompras, ":ID"=>$id));
    }
}