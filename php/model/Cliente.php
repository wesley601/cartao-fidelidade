<?php

class Cliente {
    private $nome;
    private $numtelefone;
    private $numcompras;
    private $idcliente;
    private $sexo;
    private $createdate;
    private $endereco;

    public function getNome(){
        return $this->nome;
    }
    public function setNome($value){
        $this->nome = $value;
    }
    public function getNumtelefone(){
        return $this->numtelefone;
    }
    public function setNumtelefone($value){
        $this->numtelefone = $value;
    }
    public function getNumcompras(){
        return $this->numcompras;
    }
    public function setNumcompras($value){
        $this->numcompras = $value;
    }
    public function getIdcliente(){
        return $this->idcliente;
    }
    public function setIdcliente($value){
        $this->idcliente = $value;
    }
    public function getSexo(){
        return $this->sexo;
    }
    public function setSexo($value){
        $this->sexo = $value;
    }
    public function getCreatedate(){
        return $this->createdate;
    }
    public function setCreatedate($value){
        $this->createdate = $value;
    }
    public function getEndereco(){
        return $this->endereco;
    }
    public function setEndereco($value){
        $this->endereco = $value;
    }

    public function setData($data){
    
        $this->setNome($data['nome']);
        $this->setNumtelefone($data['numtelefone']);
        $this->setNumcompras($data['numcompras']);
        $this->setSexo($data['sexo']);
        $this->setEndereco($data['endereco']);
        $this->setCreatedate(new DateTime($data['createdate']));
    }

    public function imprimir(){
        return json_encode(array(
            "nome"=>$this->getNome(),
            "numtelefone"=>$this->getNumtelefone(),
            "numcompras"=>$this->getNumcompras(),
            "sexo"=>$this->getSexo(),
            "endereco"=>$this->getEndereco(),
            "createdate"=>$this->getCreatedate()->format("d/m/Y H:i;s")
        ));
    }
}