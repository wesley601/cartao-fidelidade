<?php

class Sql extends PDO{

    private $dbname = 'dbfidelidade';
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '123456';
    
    private $conn;
    //private $error;
    
    public function __construct(){
        $dsn = "mysql:dbname=".$this->dbname.";host=".$this->host;
       
        $this->conn = new PDO($dsn, $this->user, $this->pass);
    }
    private function setParams($statment, $parameters = array()){

        foreach ($parameters as $key => $value) {
            $this->setParam($statment, $key, $value);
        }
    }
    private function setParam($statment, $key, $value){
        $statment->bindParam($key, $value);
    }

    //rawQuery é o comando sql que será passado, como um select, insert, etc.
    public function query($rawQuery, $params = array()){

        $stmt = $this->conn->prepare($rawQuery);
        $this->setParams($stmt, $params);
        //var_dump($stmt);
        $stmt->execute();

        return $stmt; 
    }

    public function select($rawQuery, $params = array()):array
    {

        $stmt = $this->query($rawQuery, $params);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

        
    }
}