<?php

require_once("Sql.php");
require_once("Cliente.php");

class Insert{

    
    public function inserir($cliente){
        $sql = new Sql();
        
        $results = $sql->select("CALL sp_clientes_insert(:NOME, :NUMTELEFONE, :SEXO, :ENDERECO)", array(
            ':NOME'=>$cliente->getNome(),
            ':NUMTELEFONE'=>$cliente->getNumtelefone(),
            ':SEXO'=>$cliente->getSexo(),
            ':ENDERECO'=>$cliente->getEndereco()
            ));
        
        if(count($results) > 0) {
            $cliente->setData($results[0]);
        }
    }
    public function inserirUsuarios($usuario){
        $sql = new Sql();

        $results = $sql->select("CALL sp_usuarios_insert(:NOMEUSU,:SENHAUSU, :FUNCAO)", array(
            ':NOMEUSU'=>$usuario->getNomeusu(),
            ':SENHAUSU'=>$usuario->getSenhausu(),
            ':FUNCAO'=>$usuario->getFuncao()
        ));
        /*
        if(count($results) > 0) {
            $usuario->setData($results[0]);
        }*/
    }
    public static function inserirMensagem($mensagem){
        $sql = new Sql();

        $sql->select("INSERT INTO tbmensagens (mensagem) VALUE (:MENSAGEM)", array(
            ':MENSAGEM'=>$mensagem
        ));
    }
}