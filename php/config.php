<?php

function MyAutoload($className) {
    $extension = spl_autoload_extensions();
    require_once (__DIR__.'/model/'.$className.$extension);
}
spl_autoload_extensions('.php');
spl_autoload_register('MyAutoload');

/*
spl_autoload_register(function($class_name){

    $filename = "model/".$class_name.".php";
    if(file_exists($filename)){
        require_once($filename);
    }

});
*/
//set_include_path("/var/www/html/cartao-fidelidade");
