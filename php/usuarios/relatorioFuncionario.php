<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 08/10/18
 * Time: 01:45
 */
require_once "../controler/Controler.php";
require_once "../config.php";

$usuario = new Usuario();

$c = new Controler();
$retorno = $c->relataFunc($_POST["target"], $_POST["search"]);
//echo $_POST["target"].$_POST["search"];
//echo json_encode($retorno);
?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <link rel="stylesheet" type="text/css" media="screen" href="css/relatorioFuncionarioPHP.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    </head>

    <body>

        <ul>
            <li><a href="adm.php">Home </a></li>
            <li><a href="cadastrarFunc.php">Cadastrar funcionário</a></li>
            <li><a href="relatorioUser.php">Buscar funcionário</a></li>
            <li><a class="active" href="relatorioFuncionario.php">Relatório </a></li>
            <li style="float:right"><a  href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
            <i style="float:right" class="fas fa-sign-out-alt"></i>
        </ul>

        <!-- Tabela responsiva -->
        <div class="tabela-full">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Função</th>
                        <th>Data de lançamento</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($retorno as $row): ?>
                    <tr>
                     <td><?php echo $row["idusuario"] ?></td>
                    <td><?php echo $row["nomeusu"] ?></td>
                    <td><?php echo $row["funcao"] ?></td>
                    <td><?php echo $row["createdate"] ?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </body>
</html>