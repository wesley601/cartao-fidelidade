<?php
//    session_start();
//    if($_SESSION['tipousu_sessao'] == "adm" || $_SESSION['tipousu_sessao'] == "user"){
  ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Relatórios</title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/relatorioClienteHTML.css" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>

<body>

    <ul>
        <li><a href="#home">Home</a></li>
        <li><a href="cadastrarCliente.php">Cadastrar cliente</a></li>
        <li><a class="active" href="#contact">Buscar cliente</a></li>
        <li style="float:right"><a href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
        <i style="float:right" class="fas fa-sign-out-alt"></i>
    </ul>

    <div class="box" method="POST">
        <form action="relatorioCliente.php" method="post">
            <h1>Pesquisar sobre</h1>

            <div id="radio-relatorio">
                <input id="btn-radio" type="radio" name="target" value="1" checked>
                <label>Nome do cliente</label><br>
                <input id="btn-radio" type="radio" name="target" value="2">
                <label>Número do cliente</label>
            </div>

            <input type="text" name="search" placeholder="Pesquisar...">

            <input type="submit" name="" id="Login" value="Buscar Cliente">
        </form>
    </div>

</body>

</html>
<?php
//    }
//    else
//    {
?>
    area restrita, faça o <a href="../../index.html">login</a>
<?php
//    }
?>