<?php
//    session_start();
//    if($_SESSION['tipousu_sessao'] == "adm" || $_SESSION['tipousu_sessao'] == "user"){
  ?>
<!DOCTYPE html>
<html lang="pt/br">

<head>
    <meta charset="UTF-8">
    <title>Quero 10 Nonilton!</title>
    <link rel="stylesheet" type="text/css" media="screen" href="styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/cadastrarCliente.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script type="text/javascript">
      $("#numero").mask("+55 (00) 0 0000-0000");
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>

<body>

    <ul>
        <li><a href="#home">Home</a></li>
        <li><a class="active" href="cadastrarCliente.php">Cadastrar cliente</a></li>
        <li style="float:right"><a href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
        <i style="float:right" class="fas fa-sign-out-alt"></i>
    </ul>

    <div class="box" method="POST" >
        <h1>Cadastrar Cliente</h1>
        <form action="main.php" method="post">
            <input type="text" name="nome" placeholder="Nome completo">
            <input type="text" id="numero" name="numero" placeholder="Contato">
            <input type="text" name="endereco" placeholder="Endereço">
            <div id="radio-funcionario">
                <label>Masculino</label>
                <input id="btn-radio" type="radio" name="sexo" value="M">
                <label>Feminino</label>
                <input id="btn-radio" type="radio" name="sexo" value="F">
                <input type="hidden" name="opt" value="cliente">
                </div>
            <input style="float:right" type="submit" id="Login" value="Cadastrar">
        </form>

        </form>
        <form action="relatorioClient.php" method="post">
            <input style="float:left" type="submit" name="" id="Login" value="Buscar Cliente" >
        </form>
    </div>
</body>

</html>
<?php
//    }
//    else
//    {
?>
    <!-- area restrita, faça o <a href="../../index.html">login</a> -->
<?php
//    }
?>