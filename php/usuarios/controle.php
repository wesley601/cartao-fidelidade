<?php

//  require_once "../config.php";

//  $retorno = Select::getMensagem();

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quero 10 Nonilton!</title>
    <link rel="stylesheet" type="text/css" media="screen" href="css/controle.css" />
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="styles.css" /> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

</head>

<body>

     <ul>
        <li><a href="adm.php">Home</a></li>
        <li><a class="active" href="controle.html">Controle de mensagens</a></li>
        <li style="float:right"><a href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
        <i style="float:right" class="fas fa-sign-out-alt"></i>
    </ul>

    <div class="box" method="POST">
        <h1>Controle de mensagens</h1>
        <form action="main.php" id="usrform" method="post">
        <div id="radio-controle">
            <!-- tyc = tipo de compra, V = Valor compra, N = numero de compras-->
            <input id="btn-radio" type="radio" style="float:left" name="tyc" value="V">

            <label class="valor">Valor de compra</label> <input class="btn-valor" type="number" name="valor" placeholder="Valor ou número de compras"><br>

            <input id="btn-radio" style="float:left" type="radio" name="tyc" value="N">
            <label>Número de compras</label><br>

            <input id="btn-radio" style="float:left" type="radio" name="tyc" value="Y">
            <label>Alterar mensagem</label>

            <input type="hidden" name="opt" value="mensagem">
        </div>

        <textarea cols="" rows="10" type="text" name="text" form="usrform" placeholder="Texto que o ADM vai mandar"></textarea>

        <input type="submit" name="submit" id="confirmar" value="Confirmar">


<!-- Tabela responsiva -->
<div class="tabela-full">
            <table class="table table-responsive">

              <thead>
                <tr>
                    <th>ID</th>
                    <th>Mensagem</th>
                    <th>data de lançamento</th>
                    <th>Selecionar</th>
                    <th>Deletar</th>
                </tr>
              </thead>

              <tbody>
              <?php foreach ($retorno as $row): ?>
                <tr>
                    <td><?php echo $row["idmensagem"] ?></td>
                    <td><?php echo $row["mensagem"] ?></td>
                    <td><?php echo $row["createdate"] ?></td>
                    <td>
                            <input type="hidden" name="">
                            <input type="hidden" name="">
                            <input type="submit" value="selecionar">
                    </td>
                    <!-- <td><button>selecionar</button></td> -->
                    <td>
                        <input type="hidden" name="id" value="<?php echo $row["idmensagem"] ?>">
                        <!-- <input type="hidden" name="btn" value="deletar"> -->
                        <input type="submit" name="submit-del" value="Deletar">
                    </td>
                </tr>
                <?php endforeach;?>
              </tbody>

            </table>
            <!-- Final da tabela responsiva -->

    

                </form>

</body>

</html>
