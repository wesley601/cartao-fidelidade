<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 08/10/18
 * Time: 01:45
 */
//  require_once "../controler/Controler.php";
//  require_once "../config.php";

//   $cliente = new Cliente();
//   $c = new Controler();
//   $retorno = $c->relata($_POST["target"], $_POST["search"]);
 //echo json_encode($retorno);
?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <link rel="stylesheet" type="text/css" media="screen"   href="css/relatorioClientePHP.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    </head>

    <body>
        <ul>
            <li><a href="#home">Home</a></li>
            <li><a href="cadastrarCliente.php">Cadastrar cliente</a></li>
            <li><a href="relatorioClient.php">Buscar cliente</a></li>
            <li><a class="active" href="relatorioCliente.php">Relatório</a></li>
            <li style="float:right"><a  href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
            <i style="float:right" class="fas fa-sign-out-alt"></i>
        </ul>

        <!-- Tabela responsiva -->
        <div class="tabela-full">
            <table class="table table-responsive">

              <thead>
                <tr>
                    <th>Nome</th>
                    <th>Número de telefone</th>
                    <th>Número de compras</th>
                    <th>Data de lançamento</th>
                    <th>Endereço</th>
                </tr>
              </thead>

              <tbody>
              <?php foreach ($retorno as $row): ?>
                <tr>
                    <td><?php echo $row["nome"] ?></td>
                    <td><?php echo $row["numtelefone"] ?></td>
                    <td><?php echo $row["numcompras"] ?></td>
                    <td><?php echo $row["createdate"] ?></td>
                    <td><?php echo $row["endereco"] ?></td>
                </tr>
                <?php endforeach;?>
              </tbody>

            </table>
            <!-- Final da tabela responsiva -->
        </div>
    </body>
</html>