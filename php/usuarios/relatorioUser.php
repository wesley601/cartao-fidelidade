<?php
//    session_start();
//    if($_SESSION['tipousu_sessao'] == "adm"){
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Relatorios</title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/relatorioFuncionarioHTML.css" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

</head>

<body>

    <ul>
        <li><a href="adm.php">Home </a></li>
        <li><a href="cadastrarFunc.php">Cadastrar funcionário</a></li>
        <li><a class="active" href="relatorioFuncionario.html">Buscar funcionário</a></li>
        <li style="float:right"><a href="http://localhost/cartao-fidelidade/php/logout.php">Logout</a></li>
        <i style="float:right" class="fas fa-sign-out-alt"></i>

    </ul>

    <div class="box" method="POST">
        <h1>Pesquisar sobre</h1>
        <div id="radio-relatorio">
            <input id="btn-radio" type="radio" name="cliente" value="1">
            <label>Nome do funcionário</label><br>
            <input id="btn-radio" type="radio" name="cliente" value="2">
            <label>ID do funcionário</label>
            <input type="hidden" name="opt" value="cliente">
        </div>
        <input type="text" name="" placeholder="Pesquisar">

        <form action="relatorioFuncionario.php" method="post">
            <input type="submit" name="" id="Login" value="Buscar funcionário">
        </form>
    </div>
</body>

</html>
<?php
//    }
//    else
//    {
?>
    <!-- Área restrita, faça o <a href="../../index.html">login</a> -->
<?php
//    }
 ?>