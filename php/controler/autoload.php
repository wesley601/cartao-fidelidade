<?php

function Autoloading($className) {
    $extension = spl_autoload_extensions();
    require_once(__DIR__.'/'.$className.$extension);
}
spl_autoload_extensions('.php');
spl_autoload_register('Autoloading');

/*
spl_autoload_register(function($class_name){

    $filename = "model/".$class_name.".php";
    if(file_exists($filename)){
        require_once($filename);
    }

});
*/
//set_include_path("/var/www/html/cartao-fidelidade");
