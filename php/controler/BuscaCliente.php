<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 06/10/18
 * Time: 01:47
 */
require_once(__DIR__.'/../config.php');

class BuscaCliente{
    
    //pega o id do cliente atravéz de seu numero
    public function getIdByNum($numtelefone){
        return Select::getByNum($numtelefone);
    }
    /*Retorna todo o cadastro do cliente atravéz do numero*/
    public function getClienteByNum($numtelefone){
        if($this->numExiste($numtelefone)){
            return Select::getNumtelefone($numtelefone);
        } else {
            return false;
        }
    }
    public function getClienteByNome($nome){
        return Select::search($nome);
    }

    public function numExiste($numtelefone){
        if( count(Select::getNumtelefone($numtelefone)) > 0){
            return true;
        } else {
            return false;
        }
    }
    public function listAll(){
        return Select::getList();
    }
}