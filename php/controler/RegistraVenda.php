<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 02/10/18
 * Time: 15:36
 */

require_once (__DIR__.'/../config.php');
require_once ("EnviaMensagem.php");
class RegistraVenda{

    //Emcrementa o numero de compras feitas
    public function IncrementaCompra($id){
        //$insert = new Insert();

        $valor_compra = Select::getCompras($id);
        $new_valor_compra = ($valor_compra[0]["numcompras"]) + 1;

        Update::alteracaoCompras($id, $new_valor_compra);
        if($new_valor_compra > 4){
            $numero = Select::getFone($id);

            EnviaMensagem::envia($numero[0]["numtelefone"], Select::getMensagem());
            Update::alteracaoCompras($id, 0);
        }
        //$insert->inserir();
    }
}