<?php

require_once(__DIR__.'/../config.php');

class BuscaUsuario {

    public function UserExist($id)
    {
        if( count(Select::getUsuario($id)) > 0){
            return true;
        } else {
            return false;
        }
    }
    public function validaUsu($id, $usuario){

        if($this->UserExist($id)){
            $results = Select::getUsuario($id);
            
            $usuario->setData($results[0]);
        } else {
            return false;
        }
    }
    public function getUsuarioById($id){
        return Select::getUsuario($id);
    }
    public function getUsuarioByNome($nome){
        return Select::searchUser($nome);
    }

}